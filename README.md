## Weather App

**1 provide api keys for weather and city providers**
in appConfig.js

Get API keys here:
https://www.weatherbit.io/api ,
https://developer.mapquest.com

**2 install dependencies**
$npm i

### for development

**3 run dev server, watch lint, and HMR**
$npm start

**4 go to the browser**
http://localhost:3001/

### Or

**3 build development code**
$npm run build

**4 go to dist/index.html**

### for production

**3 build production code**
$npm run build-prod

**4 go to dist/index.html**


----

airbnb styleguide is being used

to build files in /dist (development env) $npm run build
