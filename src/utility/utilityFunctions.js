export const joinLatLon = (lat, lon) => `${lat},${lon}`;

export const handleFetchErrors = (response) => {
  if ((response.status !== 200)) {
    return Promise.reject(new Error(response.status));
  }
  return response;
};

export const arraysEqual = (array1, array2) => (
  array1.length === array2.length
  && array1.every((value, index) => value === array2[index])
);
