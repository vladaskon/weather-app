class Weather {
  constructor(city, icon, description, temp) {
    this.city = city;
    this.icon = icon;
    this.description = description;
    this.temp = temp;
  }

  getWeather() {
    return this.weather;
  }
}

export default Weather;
