import Weather from '../classes/Weather';
import { handleFetchErrors } from '../utility/utilityFunctions';

class WeatherbitWwatherProvider {
  constructor(apiKey) {
    this.apiKey = apiKey;
  }

  fetchWeather(city) {
    return fetch(`https://api.weatherbit.io/v2.0/current?city=${city}&key=${this.apiKey}`)
      .then(handleFetchErrors)
      .then(response => response.json())
      .then((data) => {
        // eslint-disable-next-line camelcase
        const { city_name, weather, temp } = data.data[0];

        return new Weather(
          city_name,
          weather.icon,
          weather.description,
          temp,
        );
      });
  }
}

export default WeatherbitWwatherProvider;
