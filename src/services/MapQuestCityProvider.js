import { handleFetchErrors } from '../utility/utilityFunctions';

class MapQuestCityProvider {
  constructor(apiKey) {
    this.apiKey = apiKey;
  }

  fetchCity(latLon) {
    return fetch(`http://open.mapquestapi.com/geocoding/v1/address?key=${this.apiKey}&location=${latLon}`)
      .then(handleFetchErrors)
      .then(response => response.json())
      .then((data) => {
        const result = data.results[0].locations[0].adminArea5;

        if (!result) {
          return Promise.reject(new Error('no city'));
        }

        return result;
      });
  }
}

export default MapQuestCityProvider;
