import React from 'react';
import PropTypes from 'prop-types';

function SVGOverlayIcon(props) {
  const { iconClass, overlayClass } = props;

  return (
    <div className={overlayClass}>
      <div className={iconClass} />
    </div>
  );
}

SVGOverlayIcon.propTypes = {
  overlayClass: PropTypes.string.isRequired,
  iconClass: PropTypes.string.isRequired,
};

export default SVGOverlayIcon;
