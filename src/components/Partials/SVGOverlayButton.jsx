import React from 'react';
import PropTypes from 'prop-types';

function SVGOverlayButton(props) {
  const {
    buttonClass,
    overlayClass,
    onClick,
    disabled,
  } = props;

  return (
    <div className={overlayClass}>
      <button
        className={buttonClass}
        onClick={onClick}
        disabled={disabled}
        type="button"
      />
    </div>
  );
}

SVGOverlayButton.propTypes = {
  buttonClass: PropTypes.string.isRequired,
  overlayClass: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.string,
};

SVGOverlayButton.defaultProps = {
  disabled: '',
};

export default SVGOverlayButton;
