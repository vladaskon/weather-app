import React from 'react';
import PropTypes from 'prop-types';

function SVGOverlaySubmit(props) {
  const {
    buttonClass,
    overlayClass,
    onClick,
    disabled,
  } = props;

  return (
    <div className={overlayClass}>
      <input
        className={buttonClass}
        onClick={onClick}
        type="submit"
        disabled={disabled}
        value=""
      />
    </div>
  );
}

SVGOverlaySubmit.propTypes = {
  buttonClass: PropTypes.string.isRequired,
  overlayClass: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.string,
};

SVGOverlaySubmit.defaultProps = {
  disabled: '',
};

export default SVGOverlaySubmit;
