import React from 'react';
import PropTypes from 'prop-types';

function ItemWeather(props) {
  const { weather } = props;

  return (
    <React.Fragment>
      <span className="city">{weather.city}</span>
      <span className="temp">
        {weather.temp}
        &#8451;
      </span>
      <img
        className="weather-icon"
        src={`weatherIcons/${weather.icon}.png`}
        alt={weather.description}
      />
    </React.Fragment>
  );
}

ItemWeather.propTypes = {
  weather: PropTypes.shape({
    city: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    temp: PropTypes.number.isRequired,
  }).isRequired,
};

export default ItemWeather;
