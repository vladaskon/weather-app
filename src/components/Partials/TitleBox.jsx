import React from 'react';
import PropTypes from 'prop-types';

function TitleBox(props) {
  const { title } = props;

  return <h1>{title}</h1>;
}

TitleBox.propTypes = {
  title: PropTypes.string.isRequired,
};

export default TitleBox;
