import React from 'react';
import PropTypes from 'prop-types';

class FocusedSearchInput extends React.PureComponent {
  constructor(props) {
    super(props);

    this.searchInput = React.createRef();
  }

  componentDidMount() {
    this.searchInput.current.focus();
  }

  render() {
    const { onChange, inputValue, searching } = this.props;
    const readonly = searching ? 'readonly' : false;

    return (
      <input
        onChange={onChange}
        type="search"
        value={inputValue}
        ref={this.searchInput}
        readOnly={readonly}
      />
    );
  }
}

FocusedSearchInput.propTypes = {
  onChange: PropTypes.func.isRequired,
  inputValue: PropTypes.string.isRequired,
  searching: PropTypes.bool.isRequired,
};

export default FocusedSearchInput;
