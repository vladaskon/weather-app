import React from 'react';
import PropTypes from 'prop-types';
import config from '../../../appConfig';
import MapQuestCityProvider from '../../services/MapQuestCityProvider';
import SearchForm from './SearchForm';
import SelectedItem from './SelectedItem';
import { joinLatLon } from '../../utility/utilityFunctions';

const cityProvider = new MapQuestCityProvider(config.mapQuestApiKey);

const initialState = {
  errors: {},
  noGeolocation: true,
  weather: null,
};

const errorComponent = 'weatherBox';

class SearchBox extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = initialState;

    this.setWeather = this.setWeather.bind(this);
    this.setUserLocationWeather = this.setUserLocationWeather.bind(this);
    this.addCityToFavorites = this.addCityToFavorites.bind(this);
  }

  componentDidMount() {
    this.setUserLocationWeather();
  }

  setError(component, error) {
    this.setState(
      prevState => ({
        errors: Object.assign({}, prevState.errors, { [component]: error }),
      }),
    );
  }

  async setUserLocationWeather() {
    const { setHomeCity } = this.props;

    if ('geolocation' in navigator) {
      try {
        const position = await new Promise((resolve, reject) => {
          navigator.geolocation.getCurrentPosition((result) => {
            resolve(result);
          }, () => {
            this.setState({ noGeolocation: true });
            reject(new Error('blocked'));
          });
        });

        await this.setState({ noGeolocation: false });

        const { latitude, longitude } = await position.coords;

        const city = await cityProvider
          .fetchCity(joinLatLon(latitude, longitude));

        await this.setWeather(city);

        await setHomeCity(city);
      } catch (error) {
        let errorMessage;

        if (error.message === 'blocked') {
          errorMessage = 'Accessing location is blocked. Please enable it in the browser settings!';
        } else if (error.message === 'no city') {
          errorMessage = 'Could not find a city in users location!';
        } else {
          errorMessage = 'We encountered problems with city provider. Are you using valid API key?';
        }

        this.setError(errorComponent, errorMessage);
      }
    } else {
      this.setState({ noGeolocation: true });
      this.setError(errorComponent, 'Geolocation in not supported!');
    }
  }

  async setWeather(city) {
    const { fetchWeather } = this.props;

    try {
      if (!city) {
        throw new Error('blank');
      }

      const weather = await fetchWeather(city);

      await this.setState({ weather });

      await this.removeError(errorComponent);
    } catch (error) {
      let errorMessage;

      if (error.message === '403') {
        errorMessage = 'You cannot access the weather provider, are you using valid API key?';
      } else if (error.message === 'blank') {
        errorMessage = 'City name should contain letters!';
      } else {
        errorMessage = `City not found - ${city}!`;
      }

      this.setError(errorComponent, errorMessage);
    }
  }

  removeError(component) {
    this.setState(
      prevState => ({
        errors: Object.assign({}, prevState.errors, { [component]: null }),
      }),
    );
  }

  addCityToFavorites() {
    const { addCityToFavorites } = this.props;
    const { weather } = this.state;

    addCityToFavorites(weather.city);
  }

  renderSelectedItem() {
    const { favoriteLocations, homeCity } = this.props;
    const { weather } = this.state;

    if (weather) {
      return (
        <SelectedItem
          addCityToFavorites={this.addCityToFavorites}
          favoriteLocations={favoriteLocations}
          homeCity={homeCity}
          weather={weather}
        />
      );
    }

    return null;
  }

  render() {
    const { errors, noGeolocation } = this.state;
    const selectedItem = this.renderSelectedItem();

    return (
      <React.Fragment>
        <SearchForm
          setUserLocationWeather={this.setUserLocationWeather}
          errors={errors}
          noGeolocation={noGeolocation}
          setWeather={this.setWeather}
        />
        {selectedItem}
      </React.Fragment>
    );
  }
}

SearchBox.propTypes = {
  addCityToFavorites: PropTypes.func.isRequired,
  favoriteLocations: PropTypes.arrayOf(PropTypes.string).isRequired,
  fetchWeather: PropTypes.func.isRequired,
  homeCity: PropTypes.string,
  setHomeCity: PropTypes.func.isRequired,
};

SearchBox.defaultProps = {
  homeCity: null,
};

export default SearchBox;
