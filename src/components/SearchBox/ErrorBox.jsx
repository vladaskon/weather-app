import React from 'react';
import PropTypes from 'prop-types';

function ErrorBox(props) {
  const { error } = props;

  return <div className="error-box">{error}</div>;
}

ErrorBox.propTypes = {
  error: PropTypes.string.isRequired,
};

export default ErrorBox;
