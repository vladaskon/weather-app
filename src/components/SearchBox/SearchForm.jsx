import React from 'react';
import PropTypes from 'prop-types';
import FocusedSearchInput from './FocusedSearchInput';
import SVGOverlayButton from '../Partials/SVGOverlayButton';
import SVGOverlaySubmit from '../Partials/SVGOverlaySubmit';
import ErrorBox from './ErrorBox';

const errorComponent = 'weatherBox';

const initialState = {
  inputValue: '',
  searching: false,
};
class SearchForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = initialState;

    this.onInputChange = this.onInputChange.bind(this);
    this.searchWeather = this.searchWeather.bind(this);
    this.setUserLocationWeatherOnClick = this.setUserLocationWeatherOnClick
      .bind(this);
  }

  onInputChange(event) {
    const inputValue = event.target.value;

    this.setState({ inputValue });
  }

  setUserLocationWeatherOnClick() {
    const { setUserLocationWeather } = this.props;
    const { searching } = this.state;

    this.setState({ searching: true });

    if (!searching) {
      setTimeout(async () => {
        await setUserLocationWeather();
        await this.setState({
          searching: false,
          inputValue: '',
        });
      }, 500);
    }
  }

  searchWeather() {
    const { setWeather } = this.props;
    const { searching, inputValue } = this.state;

    this.setState({ searching: true });

    if (!searching) {
      setTimeout(async () => {
        const trimmedInputValue = inputValue.trim();

        await setWeather(trimmedInputValue);

        await this.setState({
          searching: false,
          inputValue: trimmedInputValue,
        });
      }, 500);
    }
  }

  renderError() {
    const { errors } = this.props;

    if (errors[errorComponent]) {
      return <ErrorBox error={errors[errorComponent]} />;
    }

    return null;
  }

  renderSearchButton() {
    const { inputValue, searching } = this.state;
    const searchDisabled = (inputValue === '' || searching) ? 'disabled' : '';

    return (
      <SVGOverlaySubmit
        buttonClass="svg-button svg-button-search"
        overlayClass={
          `svg-overlay svg-overlay-search hover ${searchDisabled}`
        }
        onClick={this.searchWeather}
        disabled={searchDisabled}
      />
    );
  }

  renderSearchInput() {
    const { inputValue, searching } = this.state;

    return (
      <FocusedSearchInput
        onChange={this.onInputChange}
        inputValue={inputValue}
        searching={searching}
      />
    );
  }

  renderHomeButton() {
    const { noGeolocation } = this.props;
    const { searching } = this.state;
    const homeDisabled = noGeolocation || searching ? 'disabled' : '';

    return (
      <SVGOverlayButton
        buttonClass="svg-button svg-button-home"
        overlayClass={`svg-overlay svg-overlay-home hover ${homeDisabled}`}
        onClick={this.setUserLocationWeatherOnClick}
        disabled={homeDisabled}
      />
    );
  }

  render() {
    const error = this.renderError();

    const searchInput = this.renderSearchInput();
    const searchButton = this.renderSearchButton();
    const homeButton = this.renderHomeButton();

    return (
      <div className="box">
        <form className="search-box">
          {searchInput}
          {searchButton}
          {homeButton}
        </form>
        {error}
      </div>
    );
  }
}

SearchForm.propTypes = {
  setUserLocationWeather: PropTypes.func.isRequired,
  errors: PropTypes.objectOf(PropTypes.string).isRequired,
  noGeolocation: PropTypes.bool.isRequired,
  setWeather: PropTypes.func.isRequired,
};

export default SearchForm;
