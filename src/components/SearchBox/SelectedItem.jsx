import React from 'react';
import PropTypes from 'prop-types';
import ItemContent from './ItemContent';
import SVGOverlayButton from '../Partials/SVGOverlayButton';

class SelectedItem extends React.PureComponent {
  renderItemContent() {
    const { weather, homeCity } = this.props;
    return (
      <ItemContent
        homeCity={homeCity}
        weather={weather}
      />
    );
  }

  renderFavoriteButton() {
    const { weather, favoriteLocations, addCityToFavorites } = this.props;
    const buttonClass = favoriteLocations.indexOf(weather.city) === -1
      ? 'svg-button-favorite-no' : 'svg-button-favorite-yes';

    return (
      <SVGOverlayButton
        buttonClass={`svg-button ${buttonClass}`}
        overlayClass="svg-overlay svg-overlay-favorite hover"
        onClick={addCityToFavorites}
      />
    );
  }

  render() {
    const itemContent = this.renderItemContent();
    const favoriteButton = this.renderFavoriteButton();

    return (
      <div className="box">
        <div className="current-city">
          {itemContent}
          {favoriteButton}
        </div>
      </div>
    );
  }
}

SelectedItem.propTypes = {
  addCityToFavorites: PropTypes.func.isRequired,
  favoriteLocations: PropTypes.arrayOf(PropTypes.string).isRequired,
  homeCity: PropTypes.string,
  weather: PropTypes.shape({
    city: PropTypes.string.isRequired,
    icon: PropTypes.string,
    description: PropTypes.string,
    temp: PropTypes.number.isRequired,
  }).isRequired,
};

SelectedItem.defaultProps = {
  homeCity: null,
};

export default SelectedItem;
