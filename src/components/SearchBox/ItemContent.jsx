import React from 'react';
import PropTypes from 'prop-types';
import SVGOverlayIcon from '../Partials/SVGOverlayIcon';
import ItemWeather from '../Partials/ItemWeather';

class ItemContent extends React.PureComponent {
  renderCurrentWeather() {
    const { weather } = this.props;

    if (weather) {
      return <ItemWeather weather={weather} />;
    }

    return null;
  }

  renderHomeIcon() {
    const { weather, homeCity } = this.props;

    if (homeCity === weather.city && homeCity !== null) {
      return (
        <SVGOverlayIcon
          overlayClass="svg-overlay svg-overlay-home"
          iconClass="svg-icon svg-icon-home"
        />
      );
    }

    return (
      <div className="empty-icon" />
    );
  }

  render() {
    const homeIcon = this.renderHomeIcon();
    const currentWeather = this.renderCurrentWeather();

    return (
      <React.Fragment>
        {homeIcon}
        <div className="current-city-inner">
          {currentWeather}
        </div>
      </React.Fragment>
    );
  }
}

ItemContent.propTypes = {
  homeCity: PropTypes.string,
  weather: PropTypes.shape({
    city: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    temp: PropTypes.number.isRequired,
  }).isRequired,
};

ItemContent.defaultProps = {
  homeCity: null,
};

export default ItemContent;
