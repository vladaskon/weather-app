import React from 'react';
import PropTypes from 'prop-types';

function ClearFavoritesButton(props) {
  const { clearFavorites } = props;

  return (
    <button
      className="button-clear-favorites"
      onClick={clearFavorites}
      type="button"
    >
      Clear Favorites
    </button>
  );
}

ClearFavoritesButton.propTypes = {
  clearFavorites: PropTypes.func.isRequired,
};

export default ClearFavoritesButton;
