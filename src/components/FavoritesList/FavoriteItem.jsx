import React from 'react';
import PropTypes from 'prop-types';
import ItemWeather from '../Partials/ItemWeather';
import SVGOverlayButton from '../Partials/SVGOverlayButton';
import SVGOverlayIcon from '../Partials/SVGOverlayIcon';

class FavoriteItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.removeCityFromFavorites = this.removeCityFromFavorites.bind(this);
  }

  removeCityFromFavorites() {
    const { removeCityFromFavorites, weather } = this.props;

    removeCityFromFavorites(weather.city);
  }

  renderHomeIcon() {
    const { homeCity, weather } = this.props;

    if (homeCity === weather.city && homeCity !== null) {
      return (
        <SVGOverlayIcon
          overlayClass="svg-overlay svg-overlay-home"
          iconClass="svg-icon svg-icon-home"
        />
      );
    }

    return <div className="empty-icon" />;
  }

  render() {
    const { weather } = this.props;
    const homeIcon = this.renderHomeIcon();

    return (
      <React.Fragment>
        {homeIcon}
        <ItemWeather weather={weather} />
        <SVGOverlayButton
          buttonClass="svg-button svg-button-delete"
          overlayClass="svg-overlay svg-overlay-delete hover"
          onClick={this.removeCityFromFavorites}
        />
      </React.Fragment>
    );
  }
}

FavoriteItem.propTypes = {
  homeCity: PropTypes.string,
  weather: PropTypes.shape({
    city: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    temp: PropTypes.number.isRequired,
  }).isRequired,
  removeCityFromFavorites: PropTypes.func.isRequired,
};

FavoriteItem.defaultProps = {
  homeCity: null,
};

export default FavoriteItem;
