import React from 'react';
import PropTypes from 'prop-types';
import FavoriteItem from './FavoriteItem';
import ClearFavoritesButton from './ClearFavoritesButton';
import { arraysEqual } from '../../utility/utilityFunctions';

const initialState = {
  favoriteLocationsWeathers: [],
};

class FavoritesList extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = initialState;
  }

  async componentDidUpdate(prevProps) {
    const { favoriteLocations } = this.props;
    if (!arraysEqual(favoriteLocations, prevProps.favoriteLocations)) {
      this.setFavoriteItemWeathers();
    }
  }

  async setFavoriteItemWeathers() {
    const { favoriteLocations, fetchWeather } = this.props;
    const favoriteLocationsWeathers = favoriteLocations
      .map(async city => fetchWeather(city));

    await Promise.all(favoriteLocationsWeathers)
      .then((weathers) => {
        this.setState({ favoriteLocationsWeathers: weathers });
      });
  }

  renderClearFavoritesButton() {
    const { clearFavorites } = this.props;
    const { favoriteLocationsWeathers } = this.state;
    if (favoriteLocationsWeathers.length > 0) {
      return (
        <ClearFavoritesButton clearFavorites={clearFavorites} />
      );
    }

    return null;
  }

  renderFavoriteItem(weather, index) {
    const { homeCity, removeCityFromFavorites } = this.props;
    return (
      <FavoriteItem
        homeCity={homeCity}
        key={index}
        weather={weather}
        removeCityFromFavorites={removeCityFromFavorites}
      />
    );
  }

  renderFavoriteItems() {
    const { favoriteLocationsWeathers } = this.state;

    return favoriteLocationsWeathers
      .map((weather, index) => this.renderFavoriteItem(weather, index));
  }

  render() {
    const clearFavoritesButton = this.renderClearFavoritesButton();
    const favoriteItems = this.renderFavoriteItems();

    return (
      <div className="box">
        <div className="favorites-box">
          {favoriteItems}
        </div>
        {clearFavoritesButton}
      </div>
    );
  }
}

FavoritesList.propTypes = {
  clearFavorites: PropTypes.func.isRequired,
  favoriteLocations: PropTypes.arrayOf(PropTypes.string).isRequired,
  fetchWeather: PropTypes.func.isRequired,
  homeCity: PropTypes.string,
  removeCityFromFavorites: PropTypes.func.isRequired,
};

FavoritesList.defaultProps = {
  homeCity: null,
};

export default FavoritesList;
