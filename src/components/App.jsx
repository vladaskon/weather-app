import React from 'react';
import config from '../../appConfig';
import WeatherbitWeatherProvider from '../services/WeatherbitWeatherProvider';
import TitleBox from './Partials/TitleBox';
import SearchBox from './SearchBox/SearchBox';
import FavoritesList from './FavoritesList/FavoritesList';

const weatherProvider = new WeatherbitWeatherProvider(
  config.weatherbitWeatherApiKey,
);

const initialState = {
  homeCity: null,
  favoriteLocations: [],
};

class App extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = initialState;

    this.addCityToFavorites = this.addCityToFavorites.bind(this);
    this.clearFavorites = this.clearFavorites.bind(this);
    this.fetchWeather = this.fetchWeather.bind(this);
    this.removeCityFromFavorites = this.removeCityFromFavorites.bind(this);
    this.setHomeCity = this.setHomeCity.bind(this);
  }

  componentDidMount() {
    this.getFavoritesFromLocalStorage();
  }

  getFavoritesFromLocalStorage() {
    const favoriteLocations = localStorage
      .getItem('favorite-weather-locations') !== null
      ? JSON.parse(localStorage.getItem('favorite-weather-locations')) : [];

    this.setState({ favoriteLocations });
  }

  setHomeCity(homeCity) {
    this.setState({ homeCity });
  }

  // eslint-disable-next-line class-methods-use-this
  async fetchWeather(city) {
    return weatherProvider.fetchWeather(city);
  }

  removeCityFromFavorites(city) {
    let { favoriteLocations } = this.state;
    favoriteLocations = favoriteLocations.filter((value) => {
      if (value !== city) {
        return city;
      }

      return null;
    });

    localStorage.setItem(
      'favorite-weather-locations',
      JSON.stringify(favoriteLocations),
    );

    this.setState({ favoriteLocations });
  }

  clearFavorites() {
    localStorage.removeItem('favorite-weather-locations');
    this.setState({ favoriteLocations: [] });
  }

  addCityToFavorites(city) {
    const { favoriteLocations } = this.state;

    if (favoriteLocations.includes(city)) {
      return;
    }

    localStorage.setItem(
      'favorite-weather-locations',
      JSON.stringify([...favoriteLocations, city]),
    );

    this.setState(prevState => ({
      favoriteLocations: [...prevState.favoriteLocations, city],
    }));
  }

  renderSearchBox() {
    const { favoriteLocations, homeCity } = this.state;
    return (
      <SearchBox
        addCityToFavorites={this.addCityToFavorites}
        favoriteLocations={favoriteLocations}
        fetchWeather={this.fetchWeather}
        homeCity={homeCity}
        setHomeCity={this.setHomeCity}
      />
    );
  }

  renderFavoritesList() {
    const { favoriteLocations, homeCity } = this.state;

    return (
      <FavoritesList
        clearFavorites={this.clearFavorites}
        favoriteLocations={favoriteLocations}
        fetchWeather={this.fetchWeather}
        homeCity={homeCity}
        removeCityFromFavorites={this.removeCityFromFavorites}
      />
    );
  }

  render() {
    const searchBox = this.renderSearchBox();
    const favoritesList = this.renderFavoritesList();

    return (
      <div className="wrapper">
        <TitleBox title="WeatherApp" />
        {searchBox}
        {favoritesList}
      </div>
    );
  }
}

export default App;
