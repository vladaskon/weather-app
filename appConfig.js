/*
  weatherbitWeatherApiKey 
  https://www.weatherbit.io/api get current weather api key for 

  mapQuestApiKey
  https://developer.mapquest.com get the api key
*/

const config = {
  weatherbitWeatherApiKey: '', 
  mapQuestApiKey: '', 
};

export default config;
